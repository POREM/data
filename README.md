# POREM data

## POREM – Politics of Remembrance

This is data produced by WWTF-funded research project
at the University of Vienna, entitled „Politics of Remembrance and 
the Transition of Public Spaces. A Political and Social Analysis of  
Vienna, 1995-2015“. For detailed information on the project and the 
involved researchers see http://porem.univie.ac.at/

The data is used for the interactive map at [porem.wien](http://porem.wien/), 
which details information on memorial sites to victims of the 
National Socialist and the Austro-Fascist regimes in the city of
Vienna, Austria.

## Data format, meta data

The data is stored in CSV format (`porem_YYYYMMDD.csv`). Each row 
represents one place of remembrance, and is described by a number of 
variables. Categorical values are abbreviated for smaller file sizes. 
The abbrevations are mapped to textual descriptions in the accompanying 
JSON file (`porem_YYYYMMDD.json`). Columns containing categorical 
values can contain multiple values.

The columns contained in the dataset are the following:

| Variable               | Category              | Value                                     | Code |
| ---------------------- | --------------------- | ----------------------------------------- | ---- |
| id                     |                       | integer                                   |      |
| name                   |                       | text                                      |      |
| year                   |                       | YYYY                                      |      |
| lat                    |                       | float, %0.6f, e.g. 48.218440              |      |
| lon                    |                       | float, %0.6f, e.g. 16.380656              |      |
| address                 |                       | text: e.g. "2., Große Pfarrgasse 1"       |      |
| theme                  |                       | Remembering                               | R    |
|                        |                       | Forgetting                                | F    |
| topic                  | social life           | Residence                                 | sr   |
|                        |                       | Education                                 | se   |
|                        |                       | Work                                      | sw   |
|                        |                       | Culture                                   | sc   |
|                        | violence              | Death&Deportation                         | vd   |
|                        |                       | Repression                                | vr   |
|                        |                       | Struggle                                  | vs   |
|                        |                       | Administration                            | va   |
| place                  | public                | street                                    | ps   |
|                        |                       | facade                                    | pf   |
|                        |                       | park                                      | pp   |
|                        | semi-public           | company                                   | sc   |
|                        |                       | building                                  | sb   |
|                        |                       | cemetery                                  | sC   |
| type                   | big                   | Monument                                  | bmo  |
|                        |                       | Memorial room                             | bme  |
|                        |                       | Exhibition                                | be   |
|                        |                       | Art installation                          | ba   |
|                        | small                 | Plaque                                    | sp   |
|                        |                       | Pavement stone                            | sps  |
|                        |                       | Street plaque                             | ssp  |
|                        |                       | Park plaque                               | spp  |
| identityOfRemembered   |                       | Opponents and resisters                   | o    |
|                        |                       | Allied and resistant soldiers             | s    |
|                        |                       | All Nazi victims                          | v    |
|                        |                       | Forced Laborers                           | fl   |
|                        |                       | Jews                                      | j    |
|                        |                       | Roma and Sinti                            | rs   |
|                        |                       | Victims of NS medicine                    | m    |
|                        |                       | Homosexuals                               | h    |
|                        |                       | Religious                                 | r    |
|                        |                       | Slovenes                                  | s    |
|                        |                       | Other                                     | O    |
| genderOfRemembered     | by name               | female                                    | nf   |
|                        |                       | male                                      | nm   |
|                        |                       | both                                      | nb   |
|                        | collective            | female                                    | cf   |
|                        |                       | male                                      | cm   |
|                        |                       | both                                      | cb   |
| formOfViolence         |                       | Establishment                             | e    |
|                        |                       | Imprisonment                              | i    |
|                        |                       | February 1934                             | f    |
|                        |                       | Deportation                               | d    |
|                        |                       | Exile                                     | E    |
|                        |                       | Death                                     | D    |
|                        |                       | Persecution                               | P    |
|                        |                       | Forced labor                              | fl   |
|                        |                       | Resistance                                | r    |
|                        |                       | Liberation                                | l    |
|                        |                       | Other                                     | o    |
|                        |                       | War                                       | w    |
|                        |                       | Deprivation                               | p    |
| formOfViolenceByRegime | Austrofascism         | Establishment                             | ae   |
|                        |                       | Imprisonment                              | ai   |
|                        |                       | Exile                                     | aE   |
|                        |                       | Death                                     | aD   |
|                        |                       | Persecution                               | ap   |
|                        |                       | Resistance                                | ar   |
|                        | Nazism                | Establishment                             | ne   |
|                        |                       | Imprisonment                              | ni   |
|                        |                       | Deprivation                               | nd   |
|                        |                       | Exile                                     | nE   |
|                        |                       | Forced Labour                             | nfl  |
|                        |                       | Death                                     | nD   |
|                        |                       | Resistance                                | nr   |
|                        |                       | Liberation                                | nl   |
| founders               |                       | Foreign states                            | f    |
|                        |                       | Federal authorities                       | a    |
|                        |                       | Municipal authorities                     | m    |
|                        |                       | Schools and universities                  | s    |
|                        |                       | Other public institutions                 | p    |
|                        |                       | Business and staff                        | b    |
|                        |                       | Parties and their survivor's associations | P    |
|                        |                       | Other survivor's associations             | S    |
|                        |                       | Remembrance associations                  | r    |
|                        |                       | Cultural associations                     | c    |
|                        |                       | Churches                                  | C    |
|                        |                       | Individuals                               | i    |
| foundersDetailed       | Foreign States        | China                                     | fc   |
|                        |                       | Israel                                    | fi   |
|                        |                       | UK                                        | fe   |
|                        |                       | USA                                       | fa   |
|                        |                       | Poland                                    | fp   |
|                        |                       | Yugoslavia                                | fy   |
|                        |                       | USSR                                      | fs   |
|                        | Federal authorities   | Government                                | ag   |
|                        |                       | Ministries                                | am   |
|                        |                       | Parliament                                | ap   |
|                        | Municipal authorities | City council                              | mc   |
|                        |                       | District council                          | md   |
|                        |                       | District museums                          | mm   |
|                        | Public institutions   | Universities                              | pu   |
|                        |                       | Schools                                   | ps   |
|                        |                       | Adult Education Center                    | pa   |
|                        |                       | DÖW                                       | pd   |
|                        |                       | Chambers                                  | pc   |
|                        | Civil society         | Business, staff, trade unions             | cb   |
|                        |                       | Socialdemocratic Party                    | cs   |
|                        |                       | People's Party                            | co   |
|                        |                       | Communist Party                           | cc   |
|                        |                       | Nonparty survivor's associations          | cn   |
|                        |                       | Remembrance associations                  | cr   |
|                        |                       | Cultural associations                     | cc   |
|                        |                       | Churches                                  | cC   |
|                        |                       | Individuals                               | ci   |
| fundingForMemento      | Federal               | National Fund of the Republic             | fn   |
|                        |                       | Future Fund of the Republic               | ff   |
|                        | Municipal             | Kunst im Öffentlichen Raum                | mk   |
